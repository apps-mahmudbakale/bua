<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8" />
    <title>Cement Market Intelligent System| Dashboard</title>
    <meta name="description" content="Updates and statistics" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="canonical" href="https://keenthemes.com/metronic" />

    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->

    <!--begin::Page Vendors Styles(used by this page)-->
    <link
        href="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.2.9"
        rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles-->


    <!--begin::Global Theme Styles(used by all pages)-->
    <link
        href="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/plugins/global/plugins.bundle.css?v=7.2.9"
        rel="stylesheet" type="text/css" />
    <link
        href="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/plugins/custom/prismjs/prismjs.bundle.css?v=7.2.9"
        rel="stylesheet" type="text/css" />
    <link href="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/css/style.bundle.css?v=7.2.9"
        rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles-->

    <!--begin::Layout Themes(used by all pages)-->
    <!--end::Layout Themes-->

    <link rel="shortcut icon" href="{{ asset('logo.jpeg') }}" />
    @livewireStyles
    <style type="text/css">
        #container {
            height: 400px;
        }

        .highcharts-figure,
        .highcharts-data-table table {
            min-width: 310px;
            max-width: 800px;
            margin: 1em auto;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #ebebeb;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

    </style>
    <script src="{{asset('code/highcharts.js')}}"></script>
    {{-- <script src="{{asset('code/modules/exporting.js')}}"></script>
    <script src="{{asset('code/modules/export-data.js')}}"></script> --}}
    <script src="{{asset('code/modules/accessibility.js')}}"></script>
</head>

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled page-loading">
    @include('_partials.header-mobile')
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-row flex-column-fluid page">
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                @include('_partials.header')
                @yield('content')
                @include('_partials.footer')
            </div>
        </div>
    </div>
    @livewireScripts
    <script
        src="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/plugins/global/plugins.bundle.js?v=7.2.9">
    </script>
    <script
        src="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/plugins/custom/prismjs/prismjs.bundle.js?v=7.2.9">
    </script>
    <script src="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/js/scripts.bundle.js?v=7.2.9">
    </script>
    <!--end::Global Theme Bundle-->

    <!--begin::Page Vendors(used by this page)-->
    <script
        src="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9">
    </script>
    <!--end::Page Vendors-->

    <!--begin::Page Scripts(used by this page)-->
    <script src="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/js/pages/widgets.js?v=7.2.9">
    </script>
    <script
        src="https://preview.keenthemes.com/metronic/theme/html/demo12/dist/assets/js/pages/custom/user/list-datatable.js?v=7.2.9">
    </script>
    <!--end::Page Scripts-->
    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <script>
        @if (session()->has('success'))
            Swal.fire({
                title: 'Success!',
                text: '{{ session()->get('success') }} !',
                icon: 'success',
                customClass: {
                    confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
            });
        @elseif (session()->has('error'))
            Swal.fire({
                title: 'Error!',
                text: '{{ session()->get('error') }} !',
                icon: 'error',
                customClass: {
                    confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
            });
        @endif
    </script>
</body>

</html>
