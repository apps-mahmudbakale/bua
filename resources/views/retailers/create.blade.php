@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Toolbar-->
        <div class="toolbar" id="kt_toolbar">
            <!--begin::Container-->
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <!--begin::Page title-->
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <!--begin::Title-->
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Retailers Records</h1>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    <!--end::Separator-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('app.dashboard') }}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted"><a href="{{ route('app.retailers.index') }}"
                                class="text-muted text-hover-primary">Retailers Record</a></li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-dark">New Record</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Post-->
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-xxl">
                <!--begin::Card-->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">New Retailer Record</h3>
                    </div>
                    <!-- /.card-header -->
                    <form action="{{ route('app.retailers.store') }}" method="POST">
                        @csrf
                        <!-- form start -->
                        <div class="card-body">
                            <div class="form-group">
                                <label>Business Name</label>
                                <input type="text" name="business_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Business Address</label>
                                <input type="text" name="business_address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Name of Owner</label>
                                <input type="text" name="owner_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Contact No</label>
                                <input type="text" name="contact" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Product</label>
                                <select name="product_id" id="" class="form-control">
                                   @foreach ($products as $product)
                                        <option value="{{$product->id}}">{{$product->product}}</option>
                                   @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <select name="state_id" id="state" class="form-control">
                                    <option value="" selected>Select State</option>
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Lga</label>
                                <select name="lga_id" id="lga" class="form-control">

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Storage Capacity</label>
                                <input type="number" name="trucks" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Daily Sales</label>
                                <input type="number" name="sales" id="daily_sales" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Projected Monthly Sales</label>
                                <input type="number" readonly name="sales_monthly" id="monthly_sales" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Projected Annually Sales</label>
                                <input type="number" readonly name="sales_annual" id="annual_sales" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Projected Annual Tonnage</label>
                                <input type="number" readonly name="annual_tonnage" id="annual_tonnage" class="form-control">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Post-->
    </div>
    <script>
        var state = document.getElementById("state");
        var lga = document.getElementById("lga");
        var daily_sales = document.getElementById("daily_sales");
        var monthly_sales = document.getElementById("monthly_sales");
        var annual_sales = document.getElementById("annual_sales");
        var annual_tonnage = document.getElementById("annual_tonnage");
        let Url = '{{ url('/api/') }}';

        lga.length = 0;

        let defaultOption = document.createElement('option');
        defaultOption.text = 'Choose LGA';

        lga.add(defaultOption);
        lga.selectedIndex = 0;

        state.addEventListener("change", function() {
            // alert(state.value);

            fetch(Url + '/getLGA/' + state.value)
                .then((res) => res.json())
                .then((data) => {
                    console.log(data)
                    document.getElementById("lga").innerHTML = "";
                    let option;
                    data.forEach(element => {
                        option = document.createElement('option');
                        option.value = element.id;
                        option.text = element.name;
                        lga.add(option)
                    });

                });
        })

        daily_sales.addEventListener('change', (()=>{
            monthly_sales.value = daily_sales.value * 30;
            annual_sales.value = monthly_sales.value * 12;
            annual_tonnage.value = annual_sales.value / 20;
        }))
    </script>
@endsection
