@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Toolbar-->
        <div class="toolbar" id="kt_toolbar">
            <!--begin::Container-->
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <!--begin::Page title-->
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <!--begin::Title-->
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Retailers Records</h1>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    <!--end::Separator-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('app.dashboard') }}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted"><a href="{{ route('app.retailers.index') }}"
                                class="text-muted text-hover-primary">End Users Record</a></li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-dark">New Record</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Post-->
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-xxl">
                <!--begin::Card-->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">New End User Record</h3>
                    </div>
                    <!-- /.card-header -->
                    <form action="{{ route('app.end-users.store') }}" method="POST">
                        @csrf
                        <!-- form start -->
                        <div class="card-body">
                            <div class="form-group">
                                <label>Business Name</label>
                                <input type="text" name="business_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Business Address</label>
                                <input type="text" name="business_address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Name of Owner</label>
                                <input type="text" name="owner_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Contact No</label>
                                <input type="text" name="contact" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Product</label>
                                <select name="product_id" id="" class="form-control">
                                   @foreach ($products as $product)
                                        <option value="{{$product->id}}">{{$product->product}}</option>
                                   @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <select name="state_id" id="state" class="form-control">
                                    <option value="" selected>Select State</option>
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Lga</label>
                                <select name="lga_id" id="lga" class="form-control">

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Daily Consumption</label>
                                <input type="number" name="consumption" id="daily_consumption" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Projected Monthly Consumption</label>
                                <input type="number" readonly name="consumption_monthly" id="monthly_consumption" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Projected Annually Consumption</label>
                                <input type="number" readonly name="consumption_annual" id="annual_consumption" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Percentage of BUA</label>
                                <input type="number" name="percentage_bua" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Percentage of Others</label>
                                <input type="number" name="percentage_others" class="form-control">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Post-->
    </div>
    <script>
        var state = document.getElementById("state");
        var lga = document.getElementById("lga");
        var daily_consumption = document.getElementById("daily_consumption");
        var monthly_consumption = document.getElementById("monthly_consumption");
        var annual_consumption = document.getElementById("annual_consumption");
        let Url = '{{ url('/api/') }}';

        lga.length = 0;

        let defaultOption = document.createElement('option');
        defaultOption.text = 'Choose LGA';

        lga.add(defaultOption);
        lga.selectedIndex = 0;

        state.addEventListener("change", function() {
            // alert(state.value);

            fetch(Url + '/getLGA/' + state.value)
                .then((res) => res.json())
                .then((data) => {
                    console.log(data)
                    document.getElementById("lga").innerHTML = "";
                    let option;
                    data.forEach(element => {
                        option = document.createElement('option');
                        option.value = element.id;
                        option.text = element.name;
                        lga.add(option)
                    });

                });
        })

        daily_consumption.addEventListener('change', (()=>{
            monthly_consumption.value = daily_consumption.value * 30;
            annual_consumption.value = monthly_consumption.value * 12;
        }))
    </script>
@endsection
