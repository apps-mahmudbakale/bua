<div>
    <div class="card mb-5 mb-xl-8">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
            <input type="text" wire:model.debounce.300ms='search' class="form-control form-control-solid w-250px ps-14" placeholder="Search user" />
            <div class="card-toolbar">
                <!--begin::Menu-->
                <button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <i class="ki-outline ki-category fs-6"></i>
                </button>
                <!--begin::Menu 2-->
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px"
                    data-kt-menu="true" style="">
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <div class="menu-content fs-6 text-dark fw-bold px-3 py-4">Quick Actions</div>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu separator-->
                    <div class="separator mb-3 opacity-75"></div>
                    <!--end::Menu separator-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">Export Users Csv</a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">Export Users Pdf</a>
                    </div>
                    <!--end::Menu item-->
                    <!--end::Menu item-->
                    <!--begin::Menu separator-->
                    <div class="separator mt-3 opacity-75"></div>
                    <!--end::Menu separator-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <div class="menu-content px-3 py-3">
                            <a href="{{route('app.coordinators.create')}}" class="btn btn-primary btn-sm px-4" href="#">Create Coordinator</a>
                        </div>
                    </div>
                    <!--end::Menu item-->
                </div>
                <!--end::Menu 2-->
                <!--end::Menu-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body py-3">
            <!--begin::Table container-->
            <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3">
                    <!--begin::Table head-->
                    <thead>
                        <tr class="fw-bold text-muted">
                            <th class="min-w-100px">S/N</th>
                            <th class="min-w-150px">First Name</th>
                            <th class="min-w-150px">Last Name</th>
                            <th class="min-w-120px">Email</th>
                            <th class="min-w-120px">Phone</th>
                            <th class="min-w-120px">Role</th>
                            <th class="min-w-100px text-end">Actions</th>
                        </tr>
                    </thead>
                    <!--end::Table head-->
                    <!--begin::Table body-->
                    <tbody>

                        @foreach ($users as $user)
                        <tr>
                            <td class="text-dark fw-bold fs-6">
                             {{ $loop->iteration }}
                            </td>
                            <td class="text-dark fw-bold fs-6">
                               {{$user->firstname}}
                            </td>
                            <td class="text-dark fw-bold fs-6">
                               {{$user->lastname}}
                            </td>
                            <td class="text-dark fw-bold fs-6">
                                {{$user->email}}
                            </td>
                            <td class="text-dark fw-bold fs-6">{{$user->phone}}</td>
                            <td>
                                <span class="badge badge-light-success">{{$user->roles->first()->name }}</span>
                            </td>
                            <td class="text-end">
                                <a href="#"
                                    class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                                    <i class="ki-outline ki-switch fs-2"></i>
                                </a>
                                <a href="{{ route('app.users.edit', $user->id) }}"
                                    class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                                    <i class="ki-outline ki-pencil fs-2"></i>
                                </a>
                                <a href="#"  id="delete{{ $user->id }}" data-value="{{ $user->id }}" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm">
                                    <i class="ki-outline ki-trash fs-2"></i>
                                </a>
                                <script>
                                    document.querySelector('#delete{{ $user->id }}').addEventListener('click', function(e) {
                                        // alert(this.getAttribute('data-value'));
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: "You won't be able to revert this!",
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Yes, delete it!'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                document.getElementById('delete#'+this.getAttribute('data-value')).submit();
                                            }
                                        })
                                    })
                                </script>
                                <form id="delete#{{ $user->id }}"
                                    action="{{ route('app.users.destroy', $user->id) }}" method="POST"
                                     style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <!--end::Table body-->
                </table>
                <!--end::Table-->
            </div>
            <!--end::Table container-->
        </div>
        <!--begin::Body-->

    </div>
</div>
