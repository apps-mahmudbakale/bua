@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Toolbar-->
        <div class="toolbar" id="kt_toolbar">
            <!--begin::Container-->
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <!--begin::Page title-->
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <!--begin::Title-->
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Dealers Records</h1>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    <!--end::Separator-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('app.dashboard') }}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted"><a href="{{ route('app.dealers.index') }}"
                                class="text-muted text-hover-primary">Dealers Records</a></li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-dark">New Record</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Post-->
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-xxl">
                <!--begin::Card-->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">New Dealer Record</h3>
                    </div>
                    <!-- /.card-header -->
                    <form action="{{ route('app.dealers.store') }}" method="POST">
                        @csrf
                        <!-- form start -->
                        <div class="card-body">
                            <div class="form-group">
                                <label>State</label>
                                <select name="state_id" id="state" class="form-control">
                                    <option value="" selected>Select State</option>
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Company's Name</label>
                                <input type="text" name="company_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Dealer's Name</label>
                                <input type="text" name="dealer_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Dealer's Office Address</label>
                                <input type="text" name="company_address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Dealer's Phone</label>
                                <input type="text" name="dealer_phone" class="form-control">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Post-->
    </div>
    <script>
        var state = document.getElementById("state");
        var lga = document.getElementById("lga");
        let Url = '{{ url('/api/') }}';

        lga.length = 0;

        let defaultOption = document.createElement('option');
        defaultOption.text = 'Choose LGA';

        lga.add(defaultOption);
        lga.selectedIndex = 0;

        state.addEventListener("change", function() {
            // alert(state.value);

            fetch(Url + '/getLGA/' + state.value)
                .then((res) => res.json())
                .then((data) => {
                    console.log(data)
                    document.getElementById("lga").innerHTML = "";
                    let option;
                    data.forEach(element => {
                        option = document.createElement('option');
                        option.value = element.id;
                        option.text = element.name;
                        lga.add(option)
                    });

                });
        })
    </script>
@endsection
