  <!--begin::Header-->
  <div id="kt_header" class="header flex-column  header-fixed ">
      <!--begin::Top-->
      <div class="header-top" style="background-color: #BD0C36">
          <!--begin::Container-->
          <div class=" container ">
              <!--begin::Left-->
              <div class="d-none d-lg-flex align-items-center mr-3">
                  <!--begin::Logo-->
                  <a href="" class="mr-10" style="background-color: white">
                      <img alt="Logo" src="{{ asset('logo.jpeg') }}" class="max-h-45px" />
                  </a>
                  <!--end::Logo-->
                  <h2 class="text-white">Market Intelligent System</h2>
              </div>
              <!--end::Left-->

              <!--begin::Topbar-->
              <div class="topbar">

                  <!--begin::User-->
                  <div class="topbar-item">
                      <div class="btn btn-icon w-auto d-flex align-items-center btn-lg px-2"
                          id="kt_quick_user_toggle">
                          <div class="d-flex text-right pr-3">
                              <span
                                  class="text-white opacity-50 font-weight-bold font-size-sm d-none d-md-inline mr-1">Hi,</span>
                              <span class="text-white font-weight-bolder font-size-sm d-none d-md-inline">{{auth()->user()->firstname." ". auth()->user()->lastname}}</span>
                          </div>
                          <span class="symbol symbol-35">
                              <span
                                  class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-15">S</span>
                          </span>
                      </div>
                  </div>
                  <!--end::User-->
              </div>
              <!--end::Topbar-->
          </div>
          <!--end::Container-->
      </div>
      <!--end::Top-->

      <!--begin::Bottom-->
      <div class="header-bottom" style="background-color: #32487c">
          <!--begin::Container-->
          <div class=" container ">
              <!--begin::Header Menu Wrapper-->
              <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                  <!--begin::Header Menu-->
                  <div id="kt_header_menu"
                      class="header-menu header-menu-left header-menu-mobile  header-menu-layout-default ">
                      <!--begin::Header Nav-->
                      <ul class="menu-nav">
                          <li class="menu-item  menu-item-active " aria-haspopup="true"><a href="{{route('app.dashboard')}}"
                                  class="menu-link "><span class="menu-text text-white">Dashboard</span></a></li>
                          <li class="menu-item" aria-haspopup="true"><a href="{{route('app.users.index')}}" class="menu-link "><span
                                      class="menu-text text-white">Users</span></a></li>
                                      <li class="menu-item" aria-haspopup="true"><a href="{{route('app.roles.index')}}" class="menu-link "><span
                                        class="menu-text text-white">Roles</span></a></li>
                          <li class="menu-item" aria-haspopup="true"><a href="{{route('app.price-monitoring.index')}}" class="menu-link "><span
                                      class="menu-text text-white">Price
                                      Monitoring</span></a></li>
                          <li class="menu-item"aria-haspopup="true"><a href="{{route('app.dealers.index')}}" class="menu-link"><span
                                      class="menu-text">Dealers Records</span></a></li>
                          <li class="menu-item" aria-haspopup="true"><a href="{{route('app.retailers.index')}}" class="menu-link "><span
                                      class="menu-text">Retailers Records</span></a></li>
                          <li class="menu-item" aria-haspopup="true"><a href="{{route('app.end-users.index')}}" class="menu-link "><span
                                      class="menu-text">End Users Records</span></a></li>
                            <li class="menu-item" aria-haspopup="true"><a href="{{route('app.settings.index')}}" class="menu-link "><span
                            class="menu-text">Settings</span></a></li>
                      </ul>
                      <!--end::Header Nav-->
                  </div>
                  <!--end::Header Menu-->
              </div>
              <!--end::Header Menu Wrapper-->

          </div>
          <!--end::Container-->
      </div>
      <!--end::Bottom-->
  </div>
  <!--end::Header-->
