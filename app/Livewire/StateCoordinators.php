<?php

namespace App\Livewire;

use Livewire\Component;

class StateCoordinators extends Component
{
    public function render()
    {
        return view('livewire.state-coordinators');
    }
}
