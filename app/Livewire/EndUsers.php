<?php

namespace App\Livewire;

use App\Models\EndUser;
use Livewire\Component;

class EndUsers extends Base
{
    public $sortBy = 'business_name';
    public function render()
    {
        if ($this->search) {
            $endusers = EndUser::query()
                ->where('business_name', 'like', '%' . $this->search . '%')
                ->paginate(10);

            return view(
                'livewire.end-users',
                ['endusers' => $endusers]
            );
        } else {
            $endusers = EndUser::orderBy($this->sortBy, $this->sortDirection)
            ->paginate(10);
            return view(
                'livewire.end-users',
                ['endusers' => $endusers]
            );
        }
    }
}
