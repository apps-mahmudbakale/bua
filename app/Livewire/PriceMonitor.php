<?php

namespace App\Livewire;

use App\Models\PriceMonitoring;
use Livewire\Component;

class PriceMonitor extends Base
{
    public $sortBy = 'product_id';
    public function render()
    {
        if ($this->search) {
            $prices = PriceMonitoring::query()
                ->where('product_id', 'like', '%' . $this->search . '%')
                ->paginate(10);

            return view(
                'livewire.price-monitor',
                ['prices' => $prices]
            );
        } else {
            $prices = PriceMonitoring::orderBy($this->sortBy, $this->sortDirection)
            ->paginate(10);
            return view(
                'livewire.price-monitor',
                ['prices' => $prices]
            );
        }
    }
}
