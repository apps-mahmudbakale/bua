<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Component;

class Coordinator extends Base
{
    public $sortBy = 'firstname';
    public function render()
    {
        if ($this->search) {
            $users = User::query()
                ->where('firstname', 'like', '%' . $this->search . '%')
                ->paginate(10);

            return view(
                'livewire.coordinator',
                ['users' => $users]
            );
        } else {
            $users = User::whereHas('roles', function ($query) {
                $query->where('name', 'coordinator');
            })->orderBy($this->sortBy, $this->sortDirection)
            ->paginate(10);
            return view(
                'livewire.coordinator',
                ['users' => $users]
            );
        }
    }
}
