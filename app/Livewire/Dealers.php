<?php

namespace App\Livewire;

use App\Models\Dealer;
use Livewire\Component;

class Dealers extends Base
{
    public $sortBy = 'company_name';
    public function render()
    {
        if ($this->search) {
            $dealers = Dealer::query()
                ->where('company_name', 'like', '%' . $this->search . '%')
                ->paginate(10);

            return view(
                'livewire.dealers',
                ['dealers' => $dealers]
            );
        } else {
            $dealers = Dealer::orderBy($this->sortBy, $this->sortDirection)
            ->paginate(10);
            return view(
                'livewire.dealers',
                ['dealers' => $dealers]
            );
        }
    }
}
