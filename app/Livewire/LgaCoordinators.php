<?php

namespace App\Livewire;

use Livewire\Component;

class LgaCoordinators extends Component
{
    public function render()
    {
        return view('livewire.lga-coordinators');
    }
}
