<?php

namespace App\Livewire;

use App\Models\Retailer;
use Livewire\Component;

class Retailers extends Base
{
    public $sortBy = 'business_name';
    public function render()
    {
        if ($this->search) {
            $retailers = Retailer::query()
                ->where('business_name', 'like', '%' . $this->search . '%')
                ->paginate(10);

            return view(
                'livewire.retailers',
                ['retailers' => $retailers]
            );
        } else {
            $retailers = Retailer::orderBy($this->sortBy, $this->sortDirection)
            ->paginate(10);
            return view(
                'livewire.retailers',
                ['retailers' => $retailers]
            );
        }
    }
}
