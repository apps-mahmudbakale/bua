<?php

namespace App\Http\Controllers;

use App\Models\State;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\PriceMonitoring;
use Illuminate\Support\Facades\DB;

class PriceMonitoringController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $wholesaleseries = [];
        $retailsaleseries = [];
        $categories = [];
        $retail_line_data = [];

        $products = Product::with('priceMonitorings')->get();

        foreach ($products as $product) {
            $wholesaleData = $product->priceMonitorings->avg('wholesale_price');
            $retailData = $product->priceMonitorings->avg('retailer_price');

            $wholesaleseries[] = [
                'name' => $product->product,
                'y' => $wholesaleData ?: 0,
                'drilldown' => $product->product,
            ];

            $retailsaleseries[] = [
                'name' => $product->product,
                'y' => $retailData ?: 0,
                'drilldown' => $product->product,
            ];
        }

        $groupedData = collect();

        foreach ($products as $product) {
            foreach ($product->priceMonitorings as $priceData) {
                $date = $priceData->created_at->toDateString();

                $groupedData->add([
                    'date' => $date,
                    'name' => $product->product,
                    'wholesale_price' => $priceData->wholesale_price,
                ]);
            }
        }

        $categories = $groupedData->pluck('date')->unique()->toArray();

        $highchartsSeries = $groupedData->groupBy('name')->map(function ($group) {
            return [
                'name' => $group->first()['name'],
                'data' => $group->pluck('wholesale_price')->toArray(),
            ];
        })->values()->toArray();


        $groupedData1 = collect();

        foreach ($products as $product) {
            foreach ($product->priceMonitorings as $priceData) {
                $date = $priceData->created_at->toDateString();

                $groupedData1->add([
                    'date' => $date,
                    'name' => $product->product,
                    'retailer_price' => $priceData->retailer_price,
                ]);
            }
        }

        $categories = $groupedData1->pluck('date')->unique()->toArray();

        $highchartsSeries1 = $groupedData1->groupBy('name')->map(function ($group) {
            return [
                'name' => $group->first()['name'],
                'data' => $group->pluck('retailer_price')->toArray(),
            ];
        })->values()->toArray();



        return view('price-monitoring.index', compact('wholesaleseries', 'retailsaleseries', 'categories', 'highchartsSeries', 'highchartsSeries1'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $states = State::all();
        $products = Product::all();
        return view('price-monitoring.create', compact('states', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $pricemonitoring = PriceMonitoring::create($request->all());
        return redirect()->route('app.price-monitoring.index')->with('success', 'Price Added');
    }

    /**
     * Display the specified resource.
     */
    public function show(PriceMonitoring $priceMonitoring)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PriceMonitoring $priceMonitoring)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PriceMonitoring $priceMonitoring)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PriceMonitoring $priceMonitoring)
    {
        $priceMonitoring->delete();
        return redirect()->route('app.price-monitoring.index')->with('success', 'Price Deleted');
    }
}
