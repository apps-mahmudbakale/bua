<?php

namespace App\Http\Controllers;

use App\Models\State;
use App\Models\EndUser;
use App\Models\Product;
use App\Models\Retailer;
use Illuminate\Http\Request;

class EndUserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $lgaconsumptions = [];
        $usersconsumptions = [];
        $endUsers = EndUser::with('product')->get();
        $percent_bua = $endUsers->where('product.product', "BUA CEMENT")->count();
        $percent_others = $endUsers->where('product.product', '!=', "BUA CEMENT")->count();
        // dd($percent_others);
        $users = EndUser::all();
        foreach ($users as $user) {
            $lgaConsumption = EndUser::where('lga_id', $user->lga->id)->sum('consumption');
            $userConsumption = EndUser::where('business_name', $user->business_name)->sum('consumption');
            $topuser = EndUser::where('business_name', $user->business_name)->sum('consumption');
            $lgaconsumptions[] = [
                'name' => $user->lga->name,
                'y' => $lgaConsumption ?: 0,
                'drilldown' => $user->lga->name,
            ];
            $usersconsumptions[] = [
                'name' => $user->business_name,
                'y' => $userConsumption ?: 0,
                'drilldown' => $user->business_name,
            ];

        }
        return view('end-users.index', compact('percent_bua', 'percent_others', 'lgaconsumptions', 'usersconsumptions'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::all();
        $states = State::all();
        return view('end-users.create', compact('products', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $endUser = EndUser::create($request->all());
        return redirect()->route('app.end-users.index')->with('success', 'End User created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(EndUser $endUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EndUser $endUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, EndUser $endUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(EndUser $endUser)
    {
        $endUser->delete();
        return redirect()->route('app.end-users.index')->with('success', 'End User deleted successfully');
    }
}
