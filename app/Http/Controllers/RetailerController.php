<?php

namespace App\Http\Controllers;

use App\Livewire\Retailers;
use App\Models\State;
use App\Models\Product;
use App\Models\Retailer;
use Illuminate\Http\Request;

class RetailerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $lgasales = [];
        $retailerssales = [];
        $topretailers = [];
        $retailers = Retailer::with('product')->get();
        $percent_bua = $retailers->where('product.product', "BUA CEMENT")->count();
        $percent_others = $retailers->where('product.product', '!=', "BUA CEMENT")->count();
        $sales = Retailer::all();
        foreach ($sales as $sale) {
            $lgaSale = Retailer::where('lga_id', $sale->lga->id)->sum('sales');
            $retailerSale = Retailer::where('business_name', $sale->business_name)->sum('sales');
            $trucks = Retailer::where('business_name', $sale->business_name)->sum('trucks');
            $lgasales[] = [
                'name' => $sale->lga->name,
                'y' => $lgaSale ?: 0,
                'drilldown' => $sale->lga->name,
            ];
            $retailerssales[] = [
                'name' => $sale->business_name,
                'y' => $retailerSale ?: 0,
                'drilldown' => $sale->business_name,
            ];
            $topretailers[] = [
                'name' => $sale->owner_name,
                'y' => $trucks ?: 0,
                'drilldown' => $sale->owner_name,
            ];
        }
        // dd($lgasales);
        return view('retailers.index', compact('percent_bua', 'percent_others', 'lgasales', 'retailerssales', 'topretailers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::all();
        $states = State::all();
        return view('retailers.create', compact('products', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $retailers = Retailer::create($request->all());
        return redirect()->route('app.retailers.index')->with('success', 'Retail Record Added');
    }

    /**
     * Display the specified resource.
     */
    public function show(Retailer $retailer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Retailer $retailer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Retailer $retailer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Retailer $retailer)
    {
        $retailer->delete();
        return redirect()->route('app.retailers.index')->with('success', 'Retail Record Deleted');

    }
}
