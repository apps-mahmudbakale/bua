<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $wholesaleseries = [];
        $retailsaleseries = [];
        $categories = [];
        $retail_line_data = [];

        $products = Product::with('priceMonitorings')->get();

        foreach ($products as $product) {
            $wholesaleData = $product->priceMonitorings->avg('wholesale_price');
            $retailData = $product->priceMonitorings->avg('retailer_price');

            $wholesaleseries[] = [
                'name' => $product->product,
                'y' => $wholesaleData ?: 0,
                'drilldown' => $product->product,
            ];

            $retailsaleseries[] = [
                'name' => $product->product,
                'y' => $retailData ?: 0,
                'drilldown' => $product->product,
            ];
        }

        $groupedData = collect();

        foreach ($products as $product) {
            foreach ($product->priceMonitorings as $priceData) {
                $date = $priceData->created_at->toDateString();

                $groupedData->add([
                    'date' => $date,
                    'name' => $product->product,
                    'wholesale_price' => $priceData->wholesale_price,
                ]);
            }
        }

        $categories = $groupedData->pluck('date')->unique()->toArray();

        $highchartsSeries = $groupedData->groupBy('name')->map(function ($group) {
            return [
                'name' => $group->first()['name'],
                'data' => $group->pluck('wholesale_price')->toArray(),
            ];
        })->values()->toArray();


        $groupedData1 = collect();

        foreach ($products as $product) {
            foreach ($product->priceMonitorings as $priceData) {
                $date = $priceData->created_at->toDateString();

                $groupedData1->add([
                    'date' => $date,
                    'name' => $product->product,
                    'retailer_price' => $priceData->retailer_price,
                ]);
            }
        }

        $categories = $groupedData1->pluck('date')->unique()->toArray();

        $highchartsSeries1 = $groupedData1->groupBy('name')->map(function ($group) {
            return [
                'name' => $group->first()['name'],
                'data' => $group->pluck('retailer_price')->toArray(),
            ];
        })->values()->toArray();
        return view('dashboard', compact('wholesaleseries', 'retailsaleseries', 'categories', 'highchartsSeries', 'highchartsSeries1'));
    }
}
