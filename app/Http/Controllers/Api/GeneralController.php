<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Lga;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function getLGA($state)
    {
        $Lgas = Lga::where('state_id', $state)->get(['id', 'name']);
        return response()->json($Lgas);
    }
}
