<?php

namespace App\Models;

use App\Models\State;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Dealer extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_name',
        'company_address',
        'dealer_name',
        'dealer_phone',
        'state_id'
    ];


    public function state() {
        return $this->belongsTo(State::class);
    }
}
