<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    use HasFactory;

    protected $fillable = [
        'business_name',
        'business_address',
        'owner_name',
        'contact',
        'product_id',
        'state_id',
        'lga_id',
        'trucks',
        'sales',
        'sales_monthly',
        'sales_annual',
        'annual_tonnage',
        'percentage_bua',
        'percentage_others'
    ];

    public function state()  {
        return $this->belongsTo(State::class);
    }

    public function lga()  {
        return $this->belongsTo(Lga::class);
    }

    public function product()  {
        return $this->belongsTo(Product::class);
    }
}
