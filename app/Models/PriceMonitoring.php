<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceMonitoring extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'state_id',
        'lga_id',
        'wholesale_price',
        'retailer_price',
        'date',
    ];


    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function state(){
        return $this->belongsTo(State::class);
    }

    public function lga() {
        return $this->belongsTo(Lga::class);
    }
}
