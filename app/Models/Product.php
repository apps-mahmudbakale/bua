<?php

namespace App\Models;

use App\Models\PriceMonitoring;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'product'
    ];

    public function priceMonitorings(){
        return  $this->hasMany(PriceMonitoring::class);
    }
}
