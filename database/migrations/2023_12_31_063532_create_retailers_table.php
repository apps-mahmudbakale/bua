<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('retailers', function (Blueprint $table) {
            $table->id();
            $table->string('business_name');
            $table->string('business_address');
            $table->string('owner_name');
            $table->string('contact');
            $table->foreignId('product_id')->references('id')->on('products');
            $table->foreignId('state_id')->references('id')->on('states');
            $table->foreignId('lga_id')->references('id')->on('lgas');
            $table->integer('trucks');
            $table->integer('sales');
            $table->integer('sales_monthly');
            $table->integer('sales_annual');
            $table->integer('annual_tonnage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('retailers');
    }
};
