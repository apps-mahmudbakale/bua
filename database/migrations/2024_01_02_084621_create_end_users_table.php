<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('end_users', function (Blueprint $table) {
            $table->id();
            $table->string('business_name');
            $table->string('business_address');
            $table->string('owner_name');
            $table->string('contact');
            $table->foreignId('product_id')->references('id')->on('products');
            $table->foreignId('state_id')->references('id')->on('states');
            $table->foreignId('lga_id')->references('id')->on('lgas');
            $table->integer('consumption');
            $table->integer('consumption_monthly');
            $table->integer('consumption_annual');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('end_users');
    }
};
