<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('price_monitorings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->foreignId('lga_id')->references('id')->on('lgas')->onDelete('cascade');
            $table->integer('wholesale_price');
            $table->integer('retailer_price');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('price_monitorings');
    }
};
